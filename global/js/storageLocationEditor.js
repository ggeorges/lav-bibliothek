// custom click-out event, from http://appden.com/javascript/fun-with-custom-events-on-elements-in-mootools/
Element.Events.clickout = {
  base : 'click',  // attach click event to element
  condition : function(event) {
    event.stopPropagation();  // stop event from bubbling up
    return false;  // never run handler when clicking on element
  },
  onAdd : function(fn) {
    this.getDocument().addEvent('click', fn);
  },
  onRemove : function(fn) {
    this.getDocument().removeEvent('click', fn);
  }
};

var sectionEditor = new Class({
	Implements: Options,
	options: {
		parent: null,
		url: null,
		initial_section_name: null,
		initial_section_width: null,
		create: false
	},
	
	// references
	container: null,
	form: null,
	idField: null,
	typeCodeField: null,
	nameField: null,
	widthField: null,
	updateButton: null,
	deleteButton: null,
	
	
	initialize: function(options) {
		this.setOptions(options);
	},
	
	drawEditor: function() {
		
		// first the container
		this.container = new Element('div',{
				'class': 'section_editor'
		});
		
		// then the form
		this.form = new Element('form', {
			action: this.options.url,
			method: 'post'
		});
		this.container.adopt(this.form);
		
		// the hidden fields
		this.idField = new Element('input', {
				type: 'hidden',
				name: 'section_id',
				value: '???'
		});
		this.form.adopt(this.idField);
		this.typeCodeField = new Element('input', {
			type: 'hidden',
			name: 'section_typeCode',
			value: 4
		});
		this.form.adopt(this.typeCodeField);
		
		// the name-field
		var p = new Element('p');
		p.appendText('name: ');
		this.nameField = new Element('input', {
			type: 'text',
			size: 20,
			maxlength: 60,
			name: 'section_name',
			value: this.options.initial_section_name
		});
		p.adopt(this.nameField);
		this.form.adopt(p);
		
		// the width-field
		var p = new Element('p');
		p.appendText('width: ');
		this.widthField = new Element('input', {
			type: 'text',
			size: 2,
			maxlength: 3,
			name: 'section_width',
			value: this.options.initial_section_width
		});
		p.adopt(this.widthField);
		this.form.adopt(p);
		
		// add action buttons
		var p = new Element('p');
		p.appendText('actions: ');
		this.updateButton = new Element('input', {
			type: 'submit',
			name: 'action',
			value: 'update'
		});
		p.adopt(this.updateButton);
		this.deleteButton = new Element('input', {
			type: 'submit',
			name: 'action',
			value: 'delete',
		});
		if (this.options.create)  { 
			this.deleteButton.setProperty('disabled', 'disabled');
			this.updateButton.setProperty('value', 'create');
			this.nameField.setProperties({
				value: '',
				placeholder: this.options.initial_name_value
			});
		}
			
		p.adopt(this.deleteButton);
		this.form.adopt(p);
		p.adopt(new Element('input', {
			type: 'reset',
			value: 'cancel',
			events: {
				click: this.hide.bind(this)
			}
		}));
		this.books_button = new Element('button', {
			text: 'manage books >>',
			events: {
				click: function(event) {
					new bookSelector(event.target, {'cat': 3});
					return false;
				}
			}
				
		});
		p.adopt(this.books_button);
		this.form.adopt(p);
		
		
		// add to the document
		this.container.inject(document.body);
		
		//position out-selvers
		pos = $(this.options.parent).getCoordinates();
		this.container.setStyles({
			border: '1px solid black',
			'-moz-border-radius': '.3em',
			'border-radius': '.3em',
			'background-color': 'white',
			padding: '1em',
	    	width: '300px',
	    	position: 'absolute',
			'left': pos.left,
			'top': pos.top + pos.height
		});
		
			
	},
	
	
	loadFromServer: function() {
		var jsonRequest = new Request.JSON({url: this.options.url.replace('.html', '.json'), 
			onSuccess: function(section){
				this.nameField.setProperty('value', section.name);
				this.widthField.setProperty('value', section.width);
			}.bind(this)
		}).get();
	},
	
	show: function() {
		this.container.setStyle('display', 'block');
		$(document.body).addEvent('click', function(e) {
			if(!e.target || !$(e.target).getParents().contains(this.container)) {
				this.hide();
			}
		}.bind(this));
	},
	
	hide: function() {
		this.container.dispose();
		$(document.body).removeEvent('click');
	}
	
});

var active_section_editor = null;

window.addEvent('domready', function() {
	
	// make sections editable
	$$('a.books').each(function(el) {
		el.addEvent('click', function(e) {
			e.stopPropagation();
			
			if (active_section_editor)
				active_section_editor.hide();
			
			active_section_editor = new sectionEditor({
		    	parent: $(el),
		    	url: e.target.href,
				initial_section_name: '???',
				initial_section_width: 0
		    });
			
			active_section_editor.drawEditor();
			active_section_editor.loadFromServer();
			active_section_editor.show();
			return false;
		})
	});
	
	// allow for new additions to be made
	$$('div.compartment').each(function(el){
		el.addEvent('click', function(e) {
			e.stopPropagation();
			if (active_section_editor)
				active_section_editor.hide();
			
			id =  e.target.getProperty('data-compartment-id');
			if (! id)
				id =  e.target.getParent().getProperty('data-compartment-id');
			
			active_section_editor = new sectionEditor({
		    	parent: $(el),
		    	url: '/index.php/admin/storageLocations/' + id + '.html',
				initial_section_name: '(new)',
				initial_section_width: 5,
				create: true
		    });
			
			active_section_editor.drawEditor();
			active_section_editor.show();
			return false;
		})
	});
	
});

var bookSelector = new Class({
	
	filter: null,
	
	initialize: function(caller, filter) {
		this.caller = caller;
		this.filter = filter;
		this.requestData();
	},

	requestData: function() {
		new Request.JSON({
			url: app_url + '/ajax/physicalcopy_listing',
			onSuccess: this.drawTable.bind(this)
		}).get(this.filter);
	},

	drawTable: function(data) {
		table = new Element('table', {
			styles: {
				width: '80%',
				'table-layout': 'fixed',
			}
		});
		thead = new Element('thead');
		table.adopt(thead);
		tr = new Element('tr');
		thead.adopt(tr);
		
		[['Signature', 10], ['Author', 20], ['Title', 70]].each(function(title) {
			tr.adopt(new Element('th', {
				text: title[0],
				styles: {
					width: title[1]
				}
			}));
		});
		tbody = new Element('tbody');
		table.adopt(tbody);
		
		data.each(function(book) {
			
			tr = new Element('tr');
			tbody.adopt(tr);
			
			td = new Element('td', {text: book.signature});
			td.adopt(new Element('input', {
				'type': 'checkbox'
			}));
			tr.adopt(td)
			
			td = new Element('td');
			for (var aid in book.authors) {
				td.adopt(new Element('a', {
					href: app_url + "/author/" + aid,
					text: book.authors[aid]
				}));
			}
			tr.adopt(td);
			tr.adopt(new Element('td', {text: book.title}));
			
		});
		
		
		this.container = new Element('div',{
			styles: {
				position: 'absolute',
				display: 'block',
				width: '80%',
				height: '80%',
				border: '1px solid black',
				'background-color': 'white',
				'font-size': '80%',
				top:'10%',
				left:'10%'
			}
		});
		
		p = new Element('p')
		p.adopt(new Element('input', {
			'type': 'radio',
			'name': 'filter_mode',
			'checked': true
		}));
		p.appendText("stored here");
		this.option_categories = new Element('input', {
			'type': 'radio',
			'name': 'filter_mode',
		});
		p.adopt(this.option_categories);
		p.appendText("browse categories: ");
		loading = new Element('span', {text: 'loading...'});
		p.adopt(loading);
		new Request.JSON({
			url: app_url + "/ajax/category_listing",
			onSuccess: function(listing) {
				select = new Element('select',{ 
						events: {
							click: function() {
								this.option_categories.set('checked', true);
							}.bind(this),
							change: function(event) {
								alert(event.target.get('value'));
							}.bind(this)
						}
				});
				listing.each(function(category) {
					select.adopt(new Element('option', {text: category.name, value: category.id}));
				});
				select.replaces(loading);
			}.bind(this)
		}).get();
		
		
		p.adopt(new Element('input', {
			'type': 'radio',
			'name': 'filter_mode',
		}));
		p.appendText("categories: ");
		p.adopt(new Element('select'))
		
		
		this.container.adopt(p);
		this.container.adopt(table);
		
		document.body.adopt(this.container);
	},


});

