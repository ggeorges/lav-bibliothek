var InlineEditor_SelectFromList = new Class({

	// class options
	Implements: Options,
	options: {
		options_url : app_url + '/ajax/category_listing',
		save_url: app_url + '/ajax/title/' + title_id + '.json',
		categories_url: app_url + '/category/',
		content_container_display: 'inline'
	},

	// state variables
	parent: null,

	// constructor
	initialize: function(content_container, options) {

		this.parent = content_container.getParent();


		// get unmodified content container
		this.content_container = content_container;
		this.change_button = new Element('button', {
			'class': 'change',
			'text': 'change',
			events: {
				click: function(evt) {
					this.enterEditor();
					return false;
				}.bind(this)
			}
		});
		this.parent.adopt(this.change_button);

		// set options
		this.setOptions(options);

	},

	enterEditor: function() {

		// display the editor
		this.hideContentContainer();
		this.suppresChangeButton();
		this.displayLoading();
		this.loadEditor();

	},

	// gather data to display the editor to the user
	// must trigger drawEditor when done
	loadEditor: function() {

		// cascaded calls;  second must go first
		req_options = new Request.JSON({
			url: this.options.options_url,
			onFailure: this.quitEditor.bind(this),
			onSuccess: this.drawEditor.bind(this)
		});

		// this will call the req_options call and store the desired value
		// as an object state
		req_title = new Request.JSON({
			url: this.options.save_url,
			onFailure: this.quitEditor.bind(this),
			onSuccess: function(reply) {
				this.current_code = new String(reply.title.category_code);
				req_options.get();
			}.bind(this)
		});

		req_title.get();
	},

	// actually produce the editor on the user's screen
	drawEditor: function(data) {
		this.hideLoading();
		this.addEditorFields(data);
		this.addDefaultButtons();
	},

	// return to static text
	quitEditor: function() {
		this.hideLoading();
		this.removeEditorFields();
		this.removeDefaultButtons();
		this.showContentContainer();
		this.reinstateChangeButton();
		return false;
	},

	// add default buttons (always the same)
	addDefaultButtons: function() {
		this.save_button = new Element('button', {
			text: 'save',
			events: {
				click: this.save.bind(this)
			}
		});
		this.cancel_button = new Element('button', {
			text: 'cancel',
			events: {
				click: this.quitEditor.bind(this)
			}
		});
		this.parent.adopt(this.save_button);
		this.parent.adopt(this.cancel_button);

	},

	// remove default buttons
	removeDefaultButtons: function() {
		if (this.save_button) this.save_button.dispose();
		if (this.cancel_button) this.cancel_button.dispose();
	},

	// display "loading" text
	displayLoading: function() {
		if (this.loading) return;
		this.loading = new Element('span', {
			text: 'loading...'
		});
		this.parent.adopt(this.loading);
	},

	// hide the "loading..." indicator
	hideLoading: function() {
		if (! this.loading) return;
		this.loading.dispose();
		this.loading = null;
	},

	// show or hide content container, and block the "change"-button
	showContentContainer: function() {
		if (this.content_container)
			this.content_container.setStyle('display', this.options.content_container_display);
	},
	hideContentContainer: function() {
		if (this.content_container)
			this.content_container.setStyle('display', 'none');
	},

	// suppress "change"-button
	suppresChangeButton: function() {
		this.change_button.addClass('stay_hidden');
	},
	reinstateChangeButton: function() {
		this.change_button.removeClass('stay_hidden');
	},

	// get the content container - OVERWRITE THIS
	getContentContainer: function() {
		return this.parent.getElement('span');
	},

	// get the "change"-button:
	getChangeButton: function() {
		return this.parent.getElement('button.change');
	},

	// add the actual editor - OVERWRITE THIS
	addEditorFields: function(listing) {

		// create drop-down element
		select = new Element('select');

		// add menu-items
		listing.each(function(category) {

			// create option tag per line
			var option = new Element('option', {
				value: category.id,
				text: category.fullName + " (" + category.code + ")"
			});

			// mark the currently selected
			if (category.id == this.current_code)
				option.setProperty('selected', 'selected');


			select.adopt(option);

		}, this);

		// add to parent and store back-refrerence
		this.parent.adopt(select);
		this.select = select;

	},

	// remove the editor - OVERWRTITE THIS
	removeEditorFields: function() {
		if (this.select) this.select.dispose();
	},

	// store changes  - OVERWRITE THIS
	// should update the static text and call "quit-editor" when done
	save: function() {

		// get new value
		this.select.set('disabled', true);
		new_id = this.select.get('value');

		// enable "loading"-placeholder and remove controls
		this.removeDefaultButtons();
		this.displayLoading();

		// make the call
		new Request.JSON({
			url: this.options.save_url,
			onSuccess: function(resp) {

				this.hideLoading();

				// adjust select-box
				this.select.set('value', resp.title.category_code);

				// update static text
				var a = new Element('a', {
					href: this.options.category_url + resp.title.category_code,
					text: this.select.getSelected()[0].text
				});
				a.replaces(this.content_container.getElement('a'));

				// remove the editor completely
				this.quitEditor();


			}.bind(this),
			onFailure: this.quitEditor.bind(this)
		}).post({'title_category_code':new_id});

		return false;
	},
});


var InlineEditor_TitleProperty = new Class({

	// class options
	Implements: Options,
	options: {
		save_url: app_url + '/ajax/title/' + title_id + '.json',
		content_container_display: 'inline'
	},

	// state variables
	parent: null,

	// constructor
	initialize: function(content_container, options) {

		this.parent = content_container.getParent();


		// get unmodified content container
		this.content_container = content_container;
		this.change_button = new Element('button', {
			'class': 'change',
			'text': 'change',
			events: {
				click: function(evt) {
					this.enterEditor();
					return false;
				}.bind(this)
			}
		});
		this.parent.adopt(this.change_button);

		// set options
		this.setOptions(options);

	},

	enterEditor: function() {

		// display the editor
		this.hideContentContainer();
		this.suppresChangeButton();
		this.displayLoading();
		this.loadEditor();

	},

	// gather data to display the editor to the user
	// must trigger drawEditor when done
	loadEditor: function() {

		// as an object state
		new Request.JSON({
			url: this.options.save_url,
			onFailure: this.quitEditor.bind(this),
			onSuccess: function(reply) {
				this.drawEditor(reply.title.title);
			}.bind(this)
		}).get();
	},


	// actually produce the editor on the user's screen
	drawEditor: function(data) {
		this.hideLoading();
		this.addEditorFields(data);
		this.addDefaultButtons();
	},

	// return to static text
	quitEditor: function() {
		this.hideLoading();
		this.removeEditorFields();
		this.removeDefaultButtons();
		this.showContentContainer();
		this.reinstateChangeButton();
		return false;
	},

	// add default buttons (always the same)
	addDefaultButtons: function() {
		this.save_button = new Element('button', {
			text: 'save',
			events: {
				click: this.save.bind(this)
			}
		});
		this.cancel_button = new Element('button', {
			text: 'cancel',
			events: {
				click: this.quitEditor.bind(this)
			}
		});
		this.parent.adopt(this.save_button);
		this.parent.adopt(this.cancel_button);

	},

	// remove default buttons
	removeDefaultButtons: function() {
		if (this.save_button) this.save_button.dispose();
		if (this.cancel_button) this.cancel_button.dispose();
	},

	// display "loading" text
	displayLoading: function() {
		if (this.loading) return;
		this.loading = new Element('span', {
			text: 'loading...'
		});
		this.parent.adopt(this.loading);
	},

	// hide the "loading..." indicator
	hideLoading: function() {
		if (! this.loading) return;
		this.loading.dispose();
		this.loading = null;
	},

	// show or hide content container, and block the "change"-button
	showContentContainer: function() {
		if (this.content_container)
			this.content_container.setStyle('display', this.options.content_container_display);
	},
	hideContentContainer: function() {
		if (this.content_container)
			this.content_container.setStyle('display', 'none');
	},

	// suppress "change"-button
	suppresChangeButton: function() {
		this.change_button.addClass('stay_hidden');
	},
	reinstateChangeButton: function() {
		this.change_button.removeClass('stay_hidden');
	},

	// add the actual editor - OVERWRITE THIS
	addEditorFields: function(title) {

		// create drop-down element
		select = new Element('input', {
			type: 'text',
			value: title,
			size: title.length
		});

		// add to parent and store back-refrerence
		this.parent.adopt(select);
		this.select = select;

	},

	// remove the editor - OVERWRTITE THIS
	removeEditorFields: function() {
		if (this.select) this.select.dispose();
	},

	// store changes  - OVERWRITE THIS
	// should update the static text and call "quit-editor" when done
	save: function() {

		// get new value
		this.select.set('disabled', true);
		new_title = this.select.get('value');

		// enable "loading"-placeholder and remove controls
		this.removeDefaultButtons();
		this.displayLoading();

		// make the call
		new Request.JSON({
			url: this.options.save_url,
			onSuccess: function(resp) {

				this.hideLoading();

				// update static text
				this.content_container.set('text', resp.title.title);
				
				// remove the editor completely
				this.quitEditor();


			}.bind(this),
			onFailure: this.quitEditor.bind(this)
		}).post({'title_title': new_title});

		return false;
	},
});