<?php

Doo::loadCore('db/DooModel');

class MiscField extends DooModel {
    public $id_misc_field_value;
    public $misc_field_id;
    public $title_id;
    public $value;
    
    protected static $_miscFieldDescription=Null;
    
    public $_table = 'misc_field_values';
    public $_primarykey = 'id_misc_field_value';
    public $_fields = array('id_misc_field_value', 'misc_field_id', 'title_id',
    		'value');
	
	function __construct() {
         parent::setupModel('MiscField');
     }
     
     public function getLabel() {
     	if (is_null(self::$_miscFieldDescription)) {
     		self::$_miscFieldDescription=array();
     		foreach(Doo::db()->find('MiscFieldDescription') as $field) {
         		self::$_miscFieldDescription[$field->id_misc_field] = $field;
         	}
     	}
     	
     	if (isset(self::$_miscFieldDescription[$this->misc_field_id])) {
     		return self::$_miscFieldDescription[$this->misc_field_id]->display_name;
     	}
     }
     
     
	
}

?>