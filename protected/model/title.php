<?php

Doo::loadCore('db/DooModel');

class Title extends DooModel {
    public $title_id;
    public $title;
    public $publication_type;
	public $category_code;

    public $_table = 'titles';
    public $_primarykey = 'title_id';
    public $_fields = array('title_id', 'title', 'publication_type', 'category_code');

	function __construct(){
         parent::setupModel('Title');
     }

	public function id() {
		return $this->title_id;
	}

	public function getAuthors() {
		$authors = Doo::db()->relate('Person', 'Title', array(
						'where'=> $this->_table . "." . $this->_primarykey . "=?",
						'param' => array($this->id())
					));
		return empty($authors) ? array() : $authors;
	}

	public function getAuthorArray() {
		$authors = $this->getAuthors();
		$ret = array();
		foreach ($authors as $author) {
			$ret[$author->id()] = $author->getFullName();
		}
		return $ret;
	}

	public function getDigitalCopies() {
		$copies = Doo::db()->relate('DigitalCopy', 'Title', array(
						'where'=> $this->_table . "." . $this->_primarykey . "=?",
						'param' => array($this->id())
					));
		return (array() ? empty($copies) : $copies);
	}

	public function getPhysicalCopies() {
		$copies = Doo::db()->relate('PhysicalCopy', 'Title', array(
						'where'=> $this->_table . "." . $this->_primarykey . "=?",
						'param' => array($this->id())
					));
		return array() ? empty($copies) : $copies;
	}

	public function getCopies($whatKind = 'all') {
		switch ($whatKind) {
			case 'digital':
				$table = 'DigitalCopy';
				break;
			case 'physical':
				$table = 'PhysicalCopy';
				break;
			case 'all':
				$copies = $this->getCopies('physical');
				return array_merge($copies, $this->getCopies('digital'));
		}

		$copies = Doo::db()->relate($table, 'Title', array(
						'where'=> $this->_table . "." . $this->_primarykey . "=?",
						'param' => array($this->id())
					));
		return (empty($copies)) ? array() : $copies;
	}

	static function findTitle($query, $category=Null) {

		# empty query -> no results!
		if (empty($query)) { return array() ; }

		# hit the database
		return Doo::db()->relate('Title', 'PhysicalCopy', array(
						'where'=> "titles.title LIKE ? OR physical_copy.signature LIKE ?",
						'param' => array("%$query%", "$query"),
						'asc' => 'title'
					));
	}

	static function getByCategory($cat) {

		return Doo::db()->find('Title', array(
					'where' => "titles.category_code = ?",
					'param' => array($cat->id()),
					'asc' => 'title'
				));

	}

	public function getCollections() {
		$copies = Doo::db()->relate('TitleCollection', 'Title', array(
				'where'=> $this->_table . "." . $this->_primarykey .
					"=? OR title_collection.parent_title=?",
				'param' => array($this->id(), $this->id())
		));
		return is_array($copies) ? $copies : array();
	}

	public function getMiscFields() {
		$copies = Doo::db()->relate('MiscField', 'Title', array(
				'where'=> $this->_table . "." . $this->_primarykey . "=?",
				'param' => array($this->id())
		));
		return is_array($copies) ? $copies : array();
	}

	public function getCategory() {
		if (! is_numeric($this->category_code) or $this->category_code < 1)
			return Null;

		Doo::loadModel('Category');
		$cat = new Category();
		$cat->category_id = $this->category_code;
		return Doo::db()->getOne($cat);
	}

	public function as_array($resolveAuthors=False) {
		return array(
			'title_id' => $this->id(),
			'title' => $this->title,
			'publication_type' => $this->publication_type,
			'category_code' => $this->category_code,
		);
	}

}

?>