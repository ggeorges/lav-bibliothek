<?php

Doo::loadCore('db/DooModel');

class Person extends DooModel {
    public $person_id;
    public $name;
    public $firstname;
	public $email;
	public $is_corporate;
	public $eth_card_id;

    public $_table = 'people';
    public $_primarykey = 'person_id';
    public $_fields = array('person_id', 'name', 'firstname', 'email', 'is_corporate', 'eth_card_id');

	function __construct(){
         parent::setupModel('Person');
     }

	 static function getPeopleWhoAuthored($title) {
		return $title->getAuthors();
	 }

	public function id() {
		return $this->person_id;
	}

	public function getFullName() {
		$name = $this->name;
		if (! empty($this->firstname))
			$name = $this->firstname . " " . $name;
		return $name;
	}

	public function getThisGuysPublications() {

		// returns all publications authored by this guy
		return Doo::db()->relate('Title', 'Person', array(
						'where'=> $this->_table . "." . $this->_primarykey . "=?",
						'param' => array($this->id())
					));
	}

	public static function searchByName($name) {
		$model = new self();
		$name .= "%";
		$results = Doo::db()->find($model, array(
			"where" => "name LIKE ? OR firstname LIKE ? OR name + ', ' + firstname LIKE ? OR firstname + ' ' + name LIKE ?",
			"param" => array($name, $name, $name, $name)

		));
		$ans = array();
		foreach($results as $result)
			array_push($ans, $result->as_array());
		return $ans;

	}

	public function as_array() {
		return array(
				"id" => $this->id(),
				"name" => $this->name,
				"firstname" => $this->firstname,
				"full_name" => $this->getFullName(),
				"corporate" => $this->is_corporate
			);
	}
}

?>