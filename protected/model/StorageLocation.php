<?php

Doo::loadCore('db/DooModel');

class StorageLocation extends DooModel {
    public $id_storage_location;
    public $display_name;
    public $parent_location;
    public $innder_index;
    public $type_code;
    public $dimension_i;
    public $dimension_j;


    public $_table = 'storage_locations';
    public $_primarykey = 'id_storage_location';
    public $_fields = array('id_storage_location', 'display_name','parent_location',
    		'inner_index', 'dimension_i', 'dimension_j', 'type_code');

    /* There are currently only four type-codes:
     * 		1: a general location, such as a building, a room etc
     * 		2: a dimension_i by dimension_j rack
     * 		3: a compartment of a type-2 storage location
     * 		4: an area within a type-3 storage location compartment; may specify
     * 			dimension_i to indicate its width (in %)
     */

	function __construct() {
         parent::setupModel('StorageLocation');
     }


    function as_array() {
    	return array(
    			'id' => $this->id(),
    			'name' => $this->display_name,
    			'parent' => $this->parent_location,
    			'inner_index' => $this->inner_index,
    			'dimension_i' => $this->dimension_i,
    			'dimension_j' => $this->dimension_j,
    			'type_code' => $this->type_code
    		);

    }

    public function id() {
    	return $this->id_storage_location;
    }

	public function turnIntoRack($n, $m) {

		# remove any existing sub-locations
		$this->deleteAllSubLocations();
		# adjust my own specs
		$this->type_code = 2;
		$this->dimension_i = $n;
		$this->dimension_j = $m;
		$this->inner_index = 0;
		$this->update();

		# add compartments
		for ($i=1; $i < ($n+1); $i++) {
			for ($j=1; $j < ($m+1); $j++) {
				$comp = new StorageLocation();
				$comp->inner_index = ($i-1)*$m + $j;
				$comp->parent_location = $this->id();
				$comp->type_code = 3;
				Doo::db()->insert($comp);
			}
		}

	}

	public function deleteAllSubLocations($including_mysel=false) {

		$needle = new StorageLocation();
		$needle->parent_location = $this->id();
		$needle = Doo::db()->find($needle);

		if (is_array($needle)) {
			foreach ($needle as $substorage)
				$substorage->deleteAllSubLocations(true);
		} else if ($needle instanceof StorageLocation) {
			$needle->deleteAllSubLocations(true);
		}
	}

	public function getParent() {
		if (! is_numeric($this->parent_location))
			return Null;
		$parent = new self();
		$parent->id_storage_location = $this->parent_location;
		return Doo::db()->find($parent)[0];

	}


	public function getParentsArray() {
		$parents = array();
		$parent = $this->getParent();
		while ($parent instanceof self) {
			array_push($parents, array('id'=>$parent->id(), 'name'=>$parent->display_name));
			$parent = $parent->getParent();
		}
		return $parents;
	}

	public function getHighestChildInnerIndex() {
		$rs = Doo::db()->query("SELECT MAX(inner_index) FROM {$this->_table} WHERE parent_location=?", array($this->id()));
		return $rs->fetchColumn(0);
	}

	public function getChildren() {

		if (! is_numeric($this->id()))
			return Null;

		$child = new self();
		$child->parent_location = $this->id();
		return Doo::db()->find($child);
	}


	public function getChildrenAsArray($include_sections=False) {
		$data = array();
		$children = $this->getChildren();
		foreach ($children as $child) {
			$sections = array();
			if ($include_sections)
				$sections = $child->getChildrenAsArray(False);
			$width = ($child->type_code==4) ? $child->dimension_i/100 : 1;
			$data[$child->inner_index] = array('id'=>$child->id(), 'name'=>$child->display_name, 'sections'=>$sections, 'width'=>$width);
		}
		return $data;
	}


}

?>