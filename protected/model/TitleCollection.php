<?php

Doo::loadCore('db/DooModel');

class TitleCollection extends DooModel {
    public $title_collection_id;
    public $display_name;
    public $collection_type;
	public $parent_title;

    public $_table = 'title_collection';
    public $_primarykey = 'title_collection_id';
    public $_fields = array('title_collection_id', 'display_name', 'collection_type', 'parent_title');
	
    function __construct(){
         parent::setupModel('TitleCollection');
     }
	 
	public function id() {
		return $this->title_collection_id;
	}
	 
	public function getTitles() {
		$copies = Doo::db()->relate('Title', 'TitleCollection', array(
						'where'=> $this->_table . "." . $this->_primarykey . "=?", 
						'param' => array($this->id())
					));
		return array() ? empty($copies) : $copies;
	}
	
	public function getParentTitle() {
		Doo::loadModel('Title');
		$title = new Title();
		$title->title_id = $this->parent_title;
		return Doo::db()->find($title, array('limit' => 1));
	}
		
}

?>