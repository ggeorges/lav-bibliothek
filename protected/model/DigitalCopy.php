<?php

Doo::loadCore('db/DooModel');

class DigitalCopy extends DooModel {
    public $id_digital_copy;
    public $title_id;
    public $url;
	
    public $_table = 'digital_copy';
    public $_primarykey = 'id_digital_copy';
    public $_fields = array('id_digital_copy', 'title_id', 'url');
	
	function __construct(){
         parent::setupModel('DigitalCopy');
     }
	
}

?>