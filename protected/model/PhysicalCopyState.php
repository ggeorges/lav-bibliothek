<?php

Doo::loadCore('db/DooModel');

class PhysicalCopyState extends DooModel {
    public $state_id;
    public $state_name;

    public $_table = 'physical_copy_states';
    public $_primarykey = 'state_id';
    public $_fields = array('state_id', 'state_name');

	function __construct() {
         parent::setupModel('PhysicalCopyState');
     }

     function as_array() {
     	return array('id' => $this->state_id, 'name'=>$this->state_name);
     }

     function id() { return $this->state_id; }

     public static function getStateArray() {

     	$s = new self();
     	$states = Doo::db()->find($s);
     	$ret = array();
     	foreach ($states as $s)
			array_push($ret, $s->as_array());
     	return $ret;
     }

}

?>