<?php

Doo::loadCore('db/DooModel');

class PhysicalCopy extends DooModel {
    public $id_physical_copy;
    public $title_id;
    public $signature;
	public $storage_location;
	public $signature_hash;

    public $_table = 'physical_copy';
    public $_primarykey = 'id_physical_copy';
    public $_fields = array('id_physical_copy', 'title_id', 'signature', 'storage_location', 'signature_hash');

	function __construct(){
         parent::setupModel('PhyiscalCopy');
     }

     function id() {
     	return $this->id_physical_copy;
     }

     public function getCurrentState() {
		Doo::loadModel('PhysicalCopyStateLink');
		$state = PhysicalCopyStateLink::getActiveState($this->id());
		return $state;
     }

     public static function getCategoryListingAsArray($category_id) {
     	if ($category_id > 0) {

     	return Doo::db()->relate('PhysicalCopy', 'Title', array(
			'where' => 'titles.category_code=?',
     		'param' => array($category_id),
     		'asc' => 'physical_copy.signature'
		));

     	} else {
     		return Doo::db()->relate('PhysicalCopy', 'Title', array(
			'asc' => array('physical_copy.storage_locations', 'physical_copy.signature_hash')
     		));
     	}
     }


     public function getTitle() {
     	Doo::loadModel('Title');
     	$title = new Title();
     	$title->title_id = $this->title_id;
     	$title = Doo::db()->getOne($title);

     	if (empty($title))
     		throw Exception("invalid title_id");

     	return $title;
     }

     public function as_array($resolve_title=False) {

     	$me = array(
     		'id' => $this->id(),
     		'barcode' => sprintf('LAV%05d', $this->id()),
     		'signature' => $this->signature,
     		'storage_location_id' => $this->storage_location,
     		'title_id' => $this->title_id
     	);

     	if ($resolve_title) {
     		if (property_exists($this, 'Title'))
     			$title = $this->Title;
     		else
     			$title = $this->getTitle();
     		$me['title'] = $title->title;
     		$me['authors'] = $title->getAuthorArray();
     	}

		return $me;
     }

     public static function browse_copies($category, $lower=null, $upper=null) {

     	// built a filter
     	$opt = array(
     		'asc' => 'physical_copy.signature',
			'where' => 'titles.category_code = ?',
			'param' => array($category)
		);

     	// add filters
     	if (! is_null($lower)) {
     		$opt['where'] .= " AND physical_copy.signature > ?";
     		array_push($opt['param'], $lower);
     	}

     	// add filters
     	if (! is_null($upper)) {
     		$opt['where'] .= " AND physical_copy.signature < ?";
     		array_push($opt['param'], $upper);
     	}

		// get stuff (efficiently, in one query)
     	$books = Doo::db()->relate('PhysicalCopy', 'Title', $opt);
     	if (empty($books)) return array();

     	// convert to array
     	$list = array();
     	foreach ($books as $book) {
     		array_push($list, $book->as_array(True));
     	}
     	return $list;

     }


}

?>