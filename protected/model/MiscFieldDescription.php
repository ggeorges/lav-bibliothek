<?php

Doo::loadCore('db/DooModel');

class MiscFieldDescription extends DooModel {
    public $id_misc_field;
    public $display_name;
    public $description;
    public $filterFunction;

    public $_table = 'misc_fields';
    public $_primarykey = 'id_misc_field';
    public $_fields = array('id_misc_field', 'display_name', 'description',
    		'filterFunction');

	function __construct(){
         parent::setupModel('MiscFieldDescription');
     }

    public function as_array() {
    	return array('name'=>$this->display_name,
    			'id'=>$this->id(),
    			'description'=>$this->description);
    }

}

?>