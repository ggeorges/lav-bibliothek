<?php

Doo::loadCore('db/DooModel');

class PhysicalCopyStateLink extends DooModel {
    public $id;
    public $physical_copy_id;
    public $state_id;
    public $starting_date;
    public $responsible;

    public $_table = 'physical_copy_state_membership';
    public $_primarykey = 'id';
    public $_fields = array('id', 'physical_copy_id', 'state_id',
    		'starting_date', 'responsible');

	function __construct() {
         parent::setupModel('PhysicalCopyStateLnk');
     }

     public function getResponsiblePerson() {
     	Doo::loadModel('Person');
     	$p = new Person();
     	$p->person_id = $this->responsible;
     	$p = Doo::db()->getOne($p);
     	return $p;
     }

	public static function getActiveState($physical_copy_id) {
		$c = new self();
		$c->physical_copy_id = $physical_copy_id;
		return Doo::db()->getOne($c, array(
			'desc' => 'starting_date'
		));

	}

	public function getState() {
		Doo::loadModel('PhysicalCopyState');
		$s = new PhysicalCopyState();
		$s->state_id = $this->state_id;
		return Doo::db()->getOne($s);
	}

	public function as_array() {
		$data = array();
		$person = $this->getResponsiblePerson();
		$data['id'] = $this->state_id;
		$data['name'] = $this->getState()->state_name;
		if (empty($person))
			$data['person']=array();
		else
			$data['person'] = array('name' => $person->getFullName(), 'id' => $person->id());
		return $data;
	}

}

?>