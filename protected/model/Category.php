<?php

Doo::loadCore('db/DooModel');

class Category extends DooModel {
    public $category_id;
    public $category_name;
    public $category_parent;
    public $category_code;
    public $standard_storage_location;


    public $_table = 'categories';
    public $_primarykey = 'category_id';
    public $_fields = array('category_id', 'category_name', 'category_parent',
    						'category_code', 'standard_storage_location');

	function __construct() {
         parent::setupModel('Category');
     }

    public function id() {
    	return $this->category_id;
    }

	public function getParent() {
		if (! is_numeric($this->category_parent))
			return Null;
		$parent = new self();
		$parent->category_id = $this->category_parent;
		return Doo::db()->find($parent)[0];
	}

	public function getParentsArray() {
		$parents = array();
		$parent = $this->getParent();
		while ($parent instanceof self) {
			array_push($parents, array('id'=>$parent->id(), 'name'=>$parent->category_name, 'code' => $parent->category_code));
			$parent = $parent->getParent();
		}
		return $parents;
	}

	public function as_array() {
		$fullName = $this->category_name;
		$code = $this->category_code;
		foreach($this->getParentsArray() as $parent) {
			if ($parent['name']=='Everything') continue;
			$fullName =$parent['name'] . ": " . $fullName ;
			$code = $parent['code'] . "-" . $code;
		}
		return array('fullName'=>$fullName,
				'id'=>$this->id(),
				'name'=>$this->category_name,
				'code'=>$code );
	}

	static function getById($id){
		# get the category object
		Doo::loadModel('Category');
		$cat = new self();
		$cat->category_id = $id;
		$cat = Doo::db()->getOne($cat);
		if (empty($cat))
			throw Exception("not found");
		return $cat;
	}

	static function getCategoryNames() {
		# get the category listing for a select box
		$cat = new self();
		$categories = array();
		foreach(Doo::db()->find($cat) as $cat) {
			array_push($categories, $cat->as_array());
		}
		usort($categories, function($a, $b) {
			return strnatcmp($a['fullName'], $b['fullName']);
		});
		return $categories;

	}

}

?>