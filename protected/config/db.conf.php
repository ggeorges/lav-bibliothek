<?php
/**
 * Example Database connection settings and DB relationship mapping
 * $dbmap[Table A]['has_one'][Table B] = array('foreign_key'=> Table B's column that links to Table A );
 * $dbmap[Table B]['belongs_to'][Table A] = array('foreign_key'=> Table A's column where Table B links to );


//Food relationship
$dbmap['Food']['belongs_to']['FoodType'] = array('foreign_key'=>'id');
$dbmap['Food']['has_many']['Article'] = array('foreign_key'=>'food_id');
$dbmap['Food']['has_one']['Recipe'] = array('foreign_key'=>'food_id');
$dbmap['Food']['has_many']['Ingredient'] = array('foreign_key'=>'food_id', 'through'=>'food_has_ingredient');

//Food Type
$dbmap['FoodType']['has_many']['Food'] = array('foreign_key'=>'food_type_id');

//Article
$dbmap['Article']['belongs_to']['Food'] = array('foreign_key'=>'id');

//Recipe
$dbmap['Recipe']['belongs_to']['Food'] = array('foreign_key'=>'id');

//Ingredient
$dbmap['Ingredient']['has_many']['Food'] = array('foreign_key'=>'ingredient_id', 'through'=>'food_has_ingredient');

*/

# title <-> author relationship
$dbmap['Title']['has_many']['Person'] = array('foreign_key' => 'title_id', 'through' => 'titles_to_authors');
$dbmap['Person']['has_many']['Title'] = array('foreign_key' => 'person_id', 'through' => 'titles_to_authors');

#title -> *_copy relationships
$dbmap['DigitalCopy']['belongs_to']['Title'] = array('foreign_key' => 'title_id');
$dbmap['Title']['has_many']['DigitalCopy'] = array('foreign_key' => 'title_id');
$dbmap['PhysicalCopy']['belongs_to']['Title'] = array('foreign_key' => 'title_id');
$dbmap['Title']['has_many']['PhysicalCopy'] = array('foreign_key' => 'title_id');

# status of physical copies
$dbmap['StatusPhysicalCopy']['has_many']['StatusPhysicalCopyLink'] = array('foreign_key' => 'state_id');
$dbmap['StatusPhysicalCopyLink']['belongs_to']['StatusPhysicalCopy'] = array('foreign_key' => 'state_id');
$dbmap['StatusPhysicalCopyLink']['belongs_to']['PhysicalCopy'] = array('foreign_key' => 'physical_copy_id');
$dbmap['PhysicalCopy']['has_many']['StatusPhysicalCopyLink'] = array('foreign_key' => 'physical_copy_id');


# title <-> title_collection
$dbmap['TitleCollection']['has_many']['Title'] = array('foreign_key' => 'title_collection_id', 'through' => 'title_collection_membership');
$dbmap['Title']['has_many']['TitleCollection'] = array('foreign_key' => 'title_id', 'through' => 'title_collection_membership');

# misc fields
$dbmap['Title']['has_many']['MiscField'] = array('foreign_key' => 'title_id');
$dbmap['MiscField']['belongs_to']['Title'] = array('foreign_key' => 'title_id');

# categories
$dbmap['Title']['belongs_to']['Category'] = array('foreign_key' => 'category_id');
$dbmap['Category']['has_many']['Title'] = array('foreign_key' => 'title_id');


//$dbconfig[ Environment or connection name] = array(Host, Database, User, Password, DB Driver, Make Persistent Connection?);
/**
 * Database settings are case sensitive.
 * To set collation and charset of the db connection, use the key 'collate' and 'charset'
 * array('localhost', 'database', 'root', '1234', 'mysql', true, 'collate'=>'utf8_unicode_ci', 'charset'=>'utf8');
 */

/* $dbconfig['dev'] = array('localhost', 'database', 'root', '1234', 'mysql', true);
 * $dbconfig['prod'] = array('localhost', 'database', 'root', '1234', 'mysql', true);
 */

 $dbconfig['dev'] = array('127.0.0.1', 'bibliothek', 'bibliothek', 'secret', 'mysql', true);

?>