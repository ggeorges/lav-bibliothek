<div class="search">
	<form name="searchbar" action="/index.php/search">
	<p class="main">
		<input type="text" name="q" size="50" />&nbsp;<button><?php echo $this->tagClassName; ?></button>
	</p>
	</form>
</div>

<!--<p class="instructions"><big>f</big>ilter by...</p>-->

<?php if (isset($this->data['noResults'])): ?>
<p>search yielded no results</p>
<?php endif; ?>

<?php if (! empty($this->data['titles'])): ?>
<?php foreach($this->data['titles'] as $id => $title): ?>
<div class="entry">
	<p><a href="/index.php/title/<?php echo $id; ?>"><?php echo $title['title']; ?></a></p>
	<?php if (isset($title['authors'])): ?>
	<p class="short-info">journal article by 
	<?php $i=0; foreach ($title['authors'] as $author_id => $author_name) { $i++; echo "<a href=\"/index.php/author/$author_id\">$author_name</a>"; echo $i==count($title['authors']) ? '' : ', '; } ?>
	</p>
	<?php endif; ?>
</div>
<?php endforeach; ?>
<?php endif; ?>