<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html SYSTEM "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head xml:lang="en">
        <title dir="ltr" lang="en">LAV Library</title>
        <link rel="stylesheet" type="text/css" href="/global/css/lav.css" />
    </head>
     <body>
         <h1><?php echo $data['title']; ?></h1>
         <table>
             <tr><th>Authors:</th><td>
                 <?php foreach($data['authors'] as $k1=>$v1): ?>
                 <a href="people/<?php echo $k1; ?>"><?php echo $v1; ?></a>,
                 <?php endforeach; ?>
             </td></tr>
         </table>
     </body>
</html>