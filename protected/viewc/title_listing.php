<!--<p class="instructions"><big>f</big>ilter by...</p>-->

<style>
table.inv {
	font-size: 9pt;
	table-layout: fixed;
	width: 100%;
}

table.inv tbody tr:hover {
	background-color: #fcc;
}

table.inv th.code {
	width: 20px;
}
table.inv th.author {
	width: 30px;
}
table.inv th.title{
	width: 100px;
}
table.inv th.state {
	width: 5px;
}

table.inv td.state {
	text-align: center;
}

table.inv tfoot td {
	text-align: right;
	border: 0;
}


ul { list-style-type: none; }
ul li { display: inline; padding-left: 5px; }


@media print {
	@page {
        size: a4 portrait;
        margin: 1.0cm;
    }
	body { margin: 0; padding: 0; font-size: 8pt; }
	div.navigation { display: none; }
	div.entry { display: none; }
	div.search { display: none; }
	p.cat-nav { display: none; }
	table.inv {
		font-size: 8pt;
		width: 20cm;
		border-spacing:0;
  		border-collapse:collapse;
  		table-layout: fixed;
  		page-break-inside:auto
  	}
  	table.inv thead{ display: table-header-group; }
  	table.inv tbody {display: table-row-group;}
  	table.inv tfoot {display: table-foot-group;}
  	table.inv tr {
		height: 0.5mm;
		page-break-inside:avoid;
		page-break-after:auto;
	}
	table.inv th.code { width: 2cm; }
	table.inv th.author { width: 3cm; }
	table.inv th.title { width: 11.5cm; }
	table.inv th.state { width: 0.5cm; }
	table.inv td {
		overflow: hidden;
		font-size: 9pt;
		border: 1pt solid black;
		font-size: 8pt;
		height: 0.5mm;
	}
	table.inv input[type=radio] {
		-webkit-appearance: none;
	    display: none;
		color: gray;
		background-color: gray;
}
	table.inv input[type=radio]:checked {
		display: inline-block;
}
	table.inv input[type=radio]:checked:after {
		content: 'x';
	    width: 100%;
	    height: 100%;
	}

table.inv td {
	overflow: hidden;
	height: 1em;
}


</style>

<script type="text/javascript">
function update(node) {
	copy_id = node.name.replace('state_', '');
	state_id = node.value;
	new Request.JSON({
		caller: $(node),
		url: '<?php echo $this->data['appurl']?>/ajax/change_inventory',
		onSuccess: function() {
			this.getParent().getParent().setStyle('background-color', '#ccc');
		}.bind(node)
	}).get({'copy_id': copy_id, 'new_state': state_id});
}

</script>
<p class="cat-nav"><input type="checkbox" />Limit to category:
<select onchange="window.location.href='<?php echo $this->data['appurl']; ?>/category/' + this.value;" class="category_choser">
<?php $cat = '--undefined--'; ?>
<?php foreach($this->data['categories'] as $category): ?>
<option value="<?php echo $category['id']; ?>"<?php if ($category['id']==$this->data['current_category']) { echo 'selected="selected"'; $cat = $category['fullName']; } ?>>
<?php echo $category['fullName']?>
</option>
<?php endforeach;?>
</select>
</p>

<table class="inv">

	<thead>
	<tr>
	<th class="author">author</th>
	<th class="title">title</th>
	</tr>
	</thead>

	<tbody><?php foreach($this->data['titles'] as $title):?>
	<tr>
		<td>
		<?php $comma=False; foreach($title['authors'] as $id => $full_name):?>
		<?php echo ($comma ? ", " : "") . $full_name; $comma=True; ?>
		<?php endforeach; ?>
		</td>
		<td><a href="<?php echo $this->data['appurl']; ?>/title/<?php echo $title['title_id']; ?>"><?php echo $title['title']; ?></a></td>
	</tr>
	<?php endforeach; ?></tbody>
</table>


