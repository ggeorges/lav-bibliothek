<!--<p class="instructions"><big>f</big>ilter by...</p>-->

<style>


ul { list-style-type: none; }
ul li { display: inline; padding-left: 5px; }

	table td.barcode {
		font-size: 8pt;
		text-align: center;
	}

	table td.barcode span {
		background-image: url(http://www.lav.ethz.ch/LAV_vortex_alone.JPG);
		background-repeat: no-repeat;
        background-position: 0 0;
        background-size: 16pt;
        padding-left: 16pt;
	}

	div.barcode {
		font-size: 8pt;
		text-align: center;
		float: left;
		width: 48.5mm;
		height: 25mm;
		vertical-align: middle;
		position: relative;
		border: 1px solid black;
	}

	div.barcode img {
		display: none;
	}

	div.barcode span {
		background-image: url(http://www.lav.ethz.ch/LAV_vortex_alone.JPG);
		background-repeat: no-repeat;
        background-position: 0 0;
        background-size: 16pt;
        padding-left: 16pt;
	}

	table.bib {
		width: 210mm;
		font-size: 9pt;
		table-layout: fixed;
		border-collapse: collapse;
		page-break-after:always;
	}
	table.bib tobdy tr { height: 26mm; }
	table.bib thead tr { height: 21mm; }
	table.bib tfoot tr { height: 21mm; }
	table.bib thead th { width: 194mm; }
	table.bib tfoot th { width: 194mm; height: 21mm; }
	table.bib thead td.gutter { width: 8mm; height: 21mm; }
	table.bib tbody td.gutter { width: 8mm; height: 26mm; }
	table.bib tfoot td.gutter { width: 8mm; height: 21mm; }
	table.bib td.sticker {width: 48.5mm; text-align: center; }


@media print {
	@page {
        size: a4 portrait;
        margin: 0;
        width: 210mm;
    }
	body { margin: 0; padding: 0; font-size: 8pt; width: 100%; }
	div.navigation { display: none; }
	div.entry { display: none; }
	div.search { display: none; }
	p.cat-nav { display: none; }
</style>

<p class="cat-nav">Available categories:
<select onchange="window.location.href='/index.php/inventory/' + this.value;" class="category_choser">
<?php $cat = ''; ?>
<?php foreach($this->data['categories'] as $category): ?>
<option value="<?php echo $category['id']; ?>"<?php if ($category['id']==$this->data['current_category']) { echo 'selected="selected"'; $cat = $category['fullName']; } ?>>
<?php echo $category['fullName']?>
</option>
<?php endforeach;?>
</select>
</p>


<table class="bib">
<thead>
<tr>
<td class="gutter"></td>
<th class="header" colspan="4"><?php echo $cat; ?></th>
<td class="gutter"></td>
</tr>
</thead>
<tbody>
<?php $counter=0; $lineCounter=0; ?>
<?php foreach($this->data['books'] as $book):?>
<?php $counter++; ?>
<?php if (($counter-1) % 4 == 0): ?>
<?php $lineCounter++;?>
<?php if ($lineCounter==11): ?>
<?php $lineCounter=1;?>
<table class="bib">
<thead>
<tr>
<td class="gutter"></td>
<th class="header" colspan="4"><?php echo $cat; ?></th>
<td class="gutter"></td>
</tr>
</thead>
<tbody>
<?php endif; ?>
<tr>
<td class="gutter"></td>
<?php endif; ?>
<td class="sticker"><span><img src="http://www.lav.ethz.ch/LAV_vortex_alone.JPG" width="16pt" /><b>library</b>: <?php echo $book['signature']?></span><br><img src="/index.php/barcode/<?php echo $book['id'] ?>" /></td>
<?php if(($counter-1) % 4 == 3): ?>
<td class="gutter"></td>
</tr>
<?php if ($lineCounter==10): ?>
</tbody>
<tfoot>
<tr>
<td class="gutter"></td>
<th colspan="4"></th>
<td class="gutter"></td>
</tr>
</tfoot>
</table>
<?php endif; ?>

<?php endif; ?>
<?php endforeach; ?>
</tbody>
</table>

