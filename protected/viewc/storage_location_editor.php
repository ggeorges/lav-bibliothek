<h1>Storage location editor</h1>

<style type="text/css">
table.storage {
	width: 80%;
	table-layout: fixed;
}

table.storage tr {
	height: 2em;
}

table.storage tr td {
	background-color: #ccc;
	border-bottom: 1px solid black;
	padding: 0em;
	vertical-align: bottom;
}

div.compartment {
	position:relative;
	width:100%;
	text-align: center;
	vertical-align: center;
}
div.compartment small {
	font-size: 80%;
	color: gray;
}

a.books {
	vertical-align: bottom;
	background-color: #555;
	margin: 0px;
	overflow: hidden;
	display: block;
	float: left;
	position: relative;
	border: 1px solid black;
	margin: -1px;
}

.selected_part {
	border: 1px solid red;
	background-color: #f55;
}

a.books:hover {
	background-color: red;
}

a.addNew {
	text-decoration: none;
	font-size: 50%;
}

a.separator {
	margin: 0;
	overflow: hidden;
	display:block;
	float: left;
}

</style>

<?php if (isset($this->data['parents'])): ?>
<p>belongs to:
<?php $count=0; ?>
<?php foreach($this->data['parents'] as $parent): ?>
<?php $count++; if ($count > 1) echo "in "; ?>
<a href="/index.php/admin/storageLocations/<?php echo $parent['id'];?>"><?php echo $parent['name'];?></a>
<?php endforeach;?>
</p>
<?php endif; ?>

<?php if (isset($this->data['name'])): ?>
<p>Name: <input type="text" size="80" value="<?php echo $this->data['name']; ?>" /></p>
<?php endif; ?>

<?php if(isset($this->data['rack'])):?>
<?php $rack = $this->data['rack']; ?>
<table class="storage">
<?php for($i=1; $i <= $rack['n']; $i++):?>
<tr>
<?php for($j=1; $j <= $rack['m']; $j++):?>
<?php $compartment=$this->data['compartments'][($i-1)*$rack['m']+$j]?>
<td<?php if ($this->data['selected_id']==$compartment['id']) echo ' class="selected_part"'?>>
<div class="compartment" data-compartment-id="<?php echo $compartment['id']; ?>">
<?php $whitespace = 0; foreach($compartment['sections'] as $section) $whitespace+=$section['width']*100; $whitespace=(100-$whitespace)/((count($compartment['sections']) > 1) ? (count($compartment['sections'])-1) : 1); $count=0;?>
<?php if ($compartment['sections']):?>
<?php foreach($compartment['sections'] as $section):?>
<?php $count++;?>
<a class="<?php if ($this->data['selected_id']==$section['id']) echo "selected_part "?>books" style="width:<?php echo $section['width']*100;?>%" href="<?php echo $section['id']?>.html" title="<?php echo ($section['name']); ?>"><?php echo empty($section['name']) ? '&nbsp;' : $section['name']; ?></a>
<?php if ($count < count($compartment['sections']) or count($compartment['sections'])==1):?>
<a class="separator" style="width:<?php echo $whitespace?>%">&nbsp;</a>
<?php endif; ?>
<?php endforeach?>
<?php else:?>
<small>(not used)</small>
<?php endif;?>
</div>
</td>
<?php endfor; ?>
</tr>
<?php endfor; ?>
</table>
<?php endif;?>
<div id="test"></div>

<script type="text/javascript" src="/global/js/storageLocationEditor.js"></script>