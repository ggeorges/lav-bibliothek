<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html SYSTEM "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head xml:lang="en">
        <title dir="ltr" lang="en">LAV library</title>
        <style type="text/css">
            body {
                margin: 0;
                font-family: sans-serif;
                width: 80%;
                margin-left: 10%;
                min-width: 500px;
                margin-left: 10%;
            }

            div.navigation {
                margin-bottom: 4em;
            }

            div.navigation ul {
                margin: 0;
                padding: .5em 0em .5em 0em;
            }

            div.navigation ul li {
                list-style: none;
                display: inline;
                margin: 0; padding: 0;
                margin-left: 1em;
                padding-left: 1em;
                font-size: 1.2em;
                letter-spacing: 5px;
            }

			div.navigation ul li a:hover {
                color: red;
            }

            div.navigation ul li.selected a {
                color: #c00;
                font-weight: bolder;
            }

            div.navigation h1 {
                font-family: sans-serif;
                margin: 0;
                padding-left: 70px;
                background-image: url(http://www.lav.ethz.ch/LAV_vortex_alone.JPG);
                background-repeat: no-repeat;
                background-position: 0 50px;
                font-size: 100px;
            }

            div.navigation h1 small {
                font-size: 0.5em;
                letter-spacing: 1em;
                font-style: italic;
                color: gray;
            }

            div.entry {
                font-size: 90%;
                margin-left: 15%;
                width: 70%;
                margin-top: 1em;
                padding: 1em;
                -moz-border-radius: .1em;
                border-radius: .1em;
                background-color: #ddd;
            }

            div.entry:hover {
                background-color: #ffcc00;
            }

            div.entry p {
                margin: 0;
                padding: 0;
            }


            div.entry p.short-info {
                margin-top: 0.2em;
                margin-left: 1em;
                font-size: 80%;
            }

            div.entry p.pubtype {
                writing-mode:tb-rl;
    	        -webkit-transform:rotate(-90deg);
            	-moz-transform:rotate(-90deg);
            	-o-transform: rotate(-90deg);
            	margin-left: -1em;
            	float:left;
            	vertical-align: bottom;
            	height:2em;
            }

            div.search {
                #background-image: url(http://images5.fanpop.com/image/photos/31000000/book-shelf-books-to-read-31060145-1064-256.png);
                #background-repeat: no-repeat;
                #background-position: 0 0px;
                background-color: #555;
                #height: 200px;
                #padding-top:140px;
                padding: 1em;
                width: 60%;
                margin-left: 20%;
                -moz-border-radius: .25em;
                border-radius: .25em;

            }

            div.search p {
                text-align: center;
                margin: 0;
                padding: 0;
            }

            div.search p.main input {
                width: 90%;
                height: 2em;
                -moz-border-radius: .5em;
                border-radius: .5em;

            }

            p.instructions {
                font-size: 80%;
                margin-top: 2em;
                margin-bottom: 2em;
                padding: 0em;
                padding-left: -0.2em;
                border-bottom: 1px solid black;
                width: 10%;
            }

            p.instructions big {
                font-family: monospace;
                font-size: 2em;
                background-color: gray;
                color: white;
                padding-left: 0.1em;
                padding-right: 0.1em;
                 -moz-border-radius: 1em;
                border-radius: .2em;
                float:left;
                margin-right: 5pt;
                margin-top: -.2em;
            }


            div.search p.filter input {

            }

        </style>
        <script src="/global/js/mootools-core.js" type="text/javascript"></script>
        <script type="text/javascript">app_url = "<?php echo $this->data['appurl']; ?>";</script>
    </head>
    <body>
        <div class="navigation">
            <h1>LAV <small>library</small></h1>
            <ul>
				<?php foreach($this->data['_navigation'] as $nav): ?>
				<li<?php echo isset($nav['is_current']) ? ' class="selected"' : '';?>><a href="<?php echo $this->data['appurl'] . $nav['link']; ?>"><?php echo $nav['display']?></a></li>
				<?php endforeach; ?>
            </ul>
        </div>
        <?php $this->inc($this->data['sub_template']); ?>

    </body>
</html>