<div class="search">
	<form name="searchbar" action="/index.php/search">
	<p class="main">
		<input type="text" name="q" size="50" />&nbsp;<button><?php echo $this->tagClassName; ?></button>
	</p>
	</form>
</div>

<!--<p class="instructions"><big>f</big>ilter by...</p>-->

<style type="text/css">
	table.editor td button.change  {
		display: none;
		height: 13pt;
		font-size: 10pt;
		background-color:#acacac;
		-moz-border-radius:3px;
		-webkit-border-radius:3px;
		border-radius:3px;
		border:1px solid #444;
	}

	table.editor td:hover button.change {
		display: inline;
	}

	table.editor td:hover button.stay_hidden {
		display: none;
	}

</style>

<script type="text/javascript">
var title_id = <?php echo $this->data['title_id']; ?>;
window.addEvent('domready', function() {
	new InlineEditor_SelectFromList($('change_category'));
	new InlineEditor_TitleProperty($('change_title'));

});

</script>

<script src="/global/js/inlineTitleEditors.js" type="text/javascript"></script>


<div>
<form name="entry" method="post">
	<table class="editor">
	<?php if (isset($this->data['compendium_member'])): ?>
	<tr>
	<td colspan="2" style="align:center;">
	This document is part of a compendium: <a href="<?php echo $this->data['compendium_member']['id']; ?>"><?php echo $this->data['compendium_member']['title']; ?></a>
	</td>
	</tr>
	<?php endif; ?>

	<tr>
	<th>Category:</th>
	<td> <?php if (! is_null($this->data['category'])):?><?php $category = $this->data['category']; ?>
	<span id="change_category"><a href="/index.php/category/<?php echo $category['id']; ?>">
	<?php echo $category['fullName']; ?> (<?php echo $category['fullCode']?>)
	</a></span>
	<?php endif;?>
	</td>
	</tr>

	<tr>
	<th>Title:</th>
	<td><span id="change_title"><?php echo $this->data['title']; ?></span></td>
	</tr>

	<tr>
	<th>Authors:</th>
	<td>
	<ul>
	<?php foreach ($this->data['authors'] as $author_id => $author_name): ?>
	<li><a href="/index.php/author/<?php echo $author_id; ?>\"><?php echo $author_name; ?></a> <button>remove</button></li>
	<?php endforeach; ?>
	<li><input type="text" /><button>add</button></li>
	</ul>
	</td>
	</tr>

	<tr>
	<th>Copies:</th>
	<td>
	<?php if (! empty($this->data['copies'])):?>
	<ul>
	<?php foreach($this->data['copies'] as $copy): ?>
	<li><?php echo $copy['signature']; ?>
	<?php if (! empty($copy['state'])): ?>
	&nbsp;&nbsp;<small><?php echo $copy['state']['name'] ?> <?php if (! empty($copy['state']['person'])):?>(<?php echo $copy['state']['person']['name'] ?>)<?php endif; ?></small>
	<?php endif;?>
	</li>
	<?php endforeach; ?>
	</ul>
	<?php endif; ?>
	</td>
	</tr>

	<?php if (! empty($this->data['versions'])): ?>
	<tr>
	<th>Versions:</th>
	<td>
	There are other versions of this document available:
	<ul>
	<?php foreach($this->data['versions'] as $version): ?>
	<li><?php echo $version['id']; ?></li>
	<?php endforeach; ?>
	</ul>
	</td>
	</tr>
	<?php endif; ?>

	<?php if (! empty($this->data['compendium_contents'])): ?>
	<tr>
	<th>Contents:</th>
	<td>
	This document is a collection of publications:
	<ul>
	<?php foreach($this->data['compendium_contents'] as $title): ?>
	<li><a href="/index.php/title/<?php echo $title['id']; ?>"><?php echo $title['title']; ?></a></li>
	<?php endforeach; ?>
	</ul>
	</td>
	</tr>
	<?php endif; ?>

	<?php if (isset($this->data['miscFields'])): ?>
	<?php foreach($this->data['miscFields'] as $field): ?>
	<tr>
	<th><?php echo $field['label']; ?>:</th>
	<td><span><?php echo $field['value']; ?></span><button>edit</button></td>
	</tr>
	<?php endforeach; endif; ?>

	</table>
</form>
</div>
