<?php
/**
 * MainController
 * Feel free to delete the methods and replace them with your own code.
 *
 * @author darkredz
 */
Doo::loadController('LAVcontroller');

class MainController extends LAVcontroller{

    public function index(){
		//Just replace these

		global $config;

		Doo::loadModel('title');
		$title = new Title();

		$route = Doo::app()->route;

		$this->renderc('search_results', array());
    }

	public function search() {

		// get stuff
		Doo::loadModel('title');

		// should be accessed by GET
		global $config;
		$data['base_path'] = $config['APP_URL'];

		// go look for titles
		$db_titles = Title::findTitle($_GET['q']);
		if (empty($db_titles)) {
			$db_titles = array();
			$data['noResults'] = true;
		}


		// init result array
		$titles = array();

		foreach ($db_titles as $title) {
			$db_authors = $title->getAuthors();
			$authors = array();
			foreach ($db_authors as $author) {
				$authors[$author->id()] = $author->fullName();
			}
			$titles[$title->id()] = array("title" => $title->title, "authors" => $authors);
		}

		$data['titles'] = $titles;

		$this->renderc('search_results', $data);
	}

	public function showTitle(){

		Doo::loadModel('title');
		Doo::loadModel('person');
		$title = new Title();
		$title->title_id = $this->params['title_id'];
		$title = Doo::db()->getOne($title);

		# no such title, quit
		if (empty($title))
			return array('/error', 'internal');

		# make self-aware...
		$data['title_id']=$title->id();

		#get and prepare authors
		$data['authors'] = $title->getAuthorArray();

		// check for collections
		$collections = $title->getCollections();
		$data['versions']=array();
		foreach($collections as $collection) {
			// get associated titles
			$collectionTitles = array();
			if (true) {
				foreach($collection->getTitles() as $_title) {
					array_push($collectionTitles, array(
					'id' => $_title->id(),
					'title' => $_title->title
					));
				}
			}

			if ($collection->collection_type==1) {

				if ($collection->parent_title==$title->id()) {
					// the title is the actual compendium
					$data['compendium_contents'] = $collectionTitles;

				} else {
					// the title is part of a compendium
					$parent = $collection->getParentTitle();
					$data['compendium_member'] = array('id'=>$parent->id(),
							'title'=>$parent->title);
				}

			} else if ($collection->collection_type==2) {
				$data['versions'] = $collectionTitles;
			}

		}

		# get and prepare copies
		$copies = $title->getCopies();
		if (! empty($copies)) {
			$data['copies']  = array();
			foreach($copies as $copy) {
					$state = Null;
					if ($copy instanceof PhysicalCopy)
						$state = $copy->getCurrentState();
						if (empty($state))
							$state = array();
						else
							$state = $state->as_array();

					array_push($data['copies'], array(
						'id' => $copy->id(),
						'signature' => $copy->signature,
						'state' => $state));
			}
		}


		$data['title'] = $title->title;

		# set the category
		$data['category'] = Null;
		$cat = $title->getCategory();
		if (! is_null($cat)) {
			$parents = $cat->getParentsArray();
			$fullCode = $cat->category_code;
			$fullName = $cat->category_name;
			foreach($parents as $parent) {
				if (! empty($parent['code'])) {
					$fullCode = $parent['code'] . "-" . $fullCode;
					$fullName = $parent['name'] . ": " . $fullName;
				}
			}

			$data['category'] = array(
						'name' => $cat->category_name,
						'fullName' => $fullName,
						'fullCode' => $fullCode,
						'id' => $cat->id()
					);

		}


		# misc fields
		$data['miscFields']=array();
		foreach($title->getMiscFields() as $field) {
			array_push($data['miscFields'], array('id'=>$field->misc_field_id,
				'label'=>$field->getLabel(), 'value'=>$field->value));
		}

		# default enconding: html
		$this->extension = (empty($this->extension)) ? '.html' : $this->extension;
		switch ($this->extension) {
			case '.json':
				$this->toJSON($data, true);
				break;
			case '.html':
				$this->renderc('title_details', $data);
				break;
			default:
				return array('/error', 'internal');
				break;
		}
	}

	public function createTitle() {
		Doo::loadModel('Category');
		$this->renderc('add_title', array(
				'categories' => Category::getCategoryNames()
			));
	}

	public function showAuthor(){

		Doo::loadModel('title');
		Doo::loadModel('person');

		$author = new Person();
		$author->person_id = $this->params['author_id'];
		$author = Doo::db()->getOne($author);

		# no such title, quit
		if (empty($author))
			return array('/error', 'internal');

		#get and prepare authors
		$titles = $author->getThisGuysPublications();
		$data['titles']  = array();
		foreach($titles as $title) {
			if ($title instanceof Title)
				$data['titles'][$title->id()] = array('title' => $title->title);
		}

		$this->renderc('search_results', $data);

	}

	public function showCategory() {

		Doo::loadModel('Category');
		try {
			$cat = Category::getById($this->params['category_id']);
		} catch (Exception $e) {
			$cat = new Category();
			$cat = Doo::db()->getOne($cat);
		}
		$this->renderc("title_listing", $this->getCategory($cat));
	}

	public function getCategory($cat) {

		# get selected category to load the listing

		Doo::loadModel('Title');
		try {
			$db_titles = Title::getByCategory($cat);
		} catch (Exception $e) {
			$db_titles = array();
		}

		// init result array
		$titles = array();
		foreach ($db_titles as $title) {
			$title_array = $title->as_array();
			$title_array['authors'] =  $title->getAuthorArray();
			array_push($titles, $title_array);
		}

		return array(
			'titles'=>$titles,
			'categories'=>Category::getCategoryNames(),
			'current_category' => $cat->id()
		);

	}

}
?>