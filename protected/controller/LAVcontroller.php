<?php
/**
 * MainController
* Common functionality for LAV layout
*
* @author Gil Georges <georges@lav.mavt.ethz.ch>
*/
class LAVcontroller extends DooController{

	public function renderc($subtemplate, $data = NULL, $enableControllerAccess = false, $includeTagClass = true) {
		$data['appurl'] = Doo::conf()->APP_URL ."index.php";
		$data['subfolder'] = Doo::conf()->SUBFOLDER;
		$data['sub_template'] = $subtemplate;
		$data['_navigation'] = array(
				array('link' => '/', 'display' => 'search', 'is_current' => true),
				array('link' => '/category', 'display' => 'browse'),
				array('link' => '/inventory/55', 'display' => 'inventory'),
				array('link' => '/admin/storageLocations/451.html', 'display'=>'storage locations'),
				array('link' => '/title/new', 'display'=>'+')
		);
		header('HTTP/1.1 200 OK');
		parent::renderc('library', $data, $enableControllerAccess, $includeTagClass);
	}

}
?>