<?php
class ajax extends DooController{

	public function getCategoryListing() {

		$listing = $this->getListing('Category', 'fullName');
		header('HTTP/1.1 200 OK');
		$this->toJSON($listing, True);


	}

	public function getMiscFieldListing() {

		$listing = $this->getListing('MiscFieldDescription', 'name');
		header('HTTP/1.1 200 OK');
		$this->toJSON($listing, True);

	}

	public function getStorageLocationListing() {
		$listing = $this->getListing('StorageLocation', 'inner_index');
		header('HTTP/1.1 200 OK');
		$this->toJSON($listing, True);
	}

	public function getPhysicalCopyListing() {

		// set the category code
		$cat = isset($_GET['cat']) ? $_GET['cat'] : 0;


		Doo::loadModel('PhysicalCopy');
		$listing = PhysicalCopy::browse_copies($cat);
		header('HTTP/1.1 200 OK');
		$this->toJSON($listing, True);

	}

	public function getAuthorListing() {
		Doo::loadModel('Person');
		$listing = array();
		if (! empty($_GET['q']))
			$listing = Person::searchByName($_GET['q']);
		header('HTTP/1.1 200 OK');
		$this->toJSON($listing, True);
	}


	private function getListing($class_name, $compare_field, $class_file_name='') {

		$class_file_name = empty($class_file_name) ? $class_name : $class_file_name;

		Doo::loadModel($class_file_name);
		$field = new  $class_name();
		$fields = array();
		foreach(Doo::db()->find($field) as $field) {
			array_push($fields, $field->as_array());
		}
		usort($fields, function($a, $b) {
			$key = array_keys($a)[0];
			return strnatcmp($a[$key], $b[$key]);
		});

		return $fields;

	}

	public function accessTitle() {

		# init
		$updateTitle = False;
		$success = True;
		$msg = array();

		# get selected title
		Doo::loadModel('title');
		$title = new Title();
		$title->title_id = $this->params['title_id'];
		$title = Doo::db()->getOne($title);

		# no such title, quit
		if (empty($title))
			return array('/error', 'internal');

		# changes?
		if (isset($_POST['title_title'])) {
			$title->title = $_POST['title_title'];
			$updateTitle = True;
		}

		if (isset($_POST['title_publication_type'])) {
			$title->publication_type = $_POST['title_publication_type'];
			$updateTitle = True;
		}

		if (isset($_POST['title_category_code'])) {

			// try to find the category
			Doo::loadModel('Category');
			$cat = new Category();
			$cat->category_id = $_POST['title_category_code'];
			$cat = Doo::db()->getOne($cat);
			if (empty($cat)) {
				$success = False;
				array_push($msg, "Category not found");
			} else {
				$title->category_code =$cat->id();
				$updateTitle = True;
			}
		}

		if ($updateTitle) {
			Doo::db()->update($title);
			array_push($msg, "title has been updated");
		}

		header('HTTP/1.1 200 OK');
		$this->toJSON(array('success' => $success, 'title'=>$title, 'msg'=>$msg), True);

	}




}
