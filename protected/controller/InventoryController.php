<?php
/**
 * MainController
 * Feel free to delete the methods and replace them with your own code.
 *
 * @author ggeorges
 **/

Doo::loadController('LAVcontroller');

class inventoryController extends LAVcontroller{

    public function showInventory() {
    	$inv = $this->getInventory($this->params['id']);
    	$this->renderc('inventory_listing',$inv);
    }

    public function showBarcodes() {
    	$inv = $this->getInventory($this->params['id']);
    	$this->renderc('inventory_barcodes',$inv);
    }

    public function changeInventoryState() {
    	header('HTTP/1.1 200 OK');
    	$this->toJSON($this->doChangeInventoryState(), true);
    }

    private function doChangeInventoryState() {


    	Doo::loadModel('PhysicalCopy');
    	$copy = new PhysicalCopy();
    	$copy->id_physical_copy = $_REQUEST['copy_id'];
    	$copy = Doo::db()->getOne($copy);
    	if (empty($copy))
    		return array('success' => False, 'msg' => 'unknown copy');

    	# get the new state form db (to make sure it exists)
    	Doo::loadModel('PhysicalCopyState');
    	$state = new PhysicalCopyState();
    	$state->state_id = $_REQUEST['new_state'];
    	$state = Doo::db()->getOne($state);
    	if (empty($state))
    		return array('success' => False, 'msg' => 'unknown state');

    	# get current state (id)
    	$current_state = $copy->getCurrentState();

    	# during the first 10 minutes, don't add a new entry
    	$use_old = False;
    	if (! empty($current_state))
    		$use_old = (time() - $current_state->starting_date) < 600;
    	if ($use_old)
			$new_state = $current_state;
		else {
			Doo::loadModel('PhysicalCopyStateLink');
			$new_state = new PhysicalCopyStateLink();
		}

		$new_state->state_id = $state->id();
		$new_state->starting_date = time();
		$new_state->responsible = 0;//$_REQUEST['responsible'];
		$new_state->physical_copy_id = $copy->id();

		if ($use_old) {
			Doo::db()->update($new_state);
		} else {
			Doo::db()->insert($new_state);
			$new_state->state_id = Doo::db()->lastInsertId();
		}

		return array(
			'success' => True,
			'state_id' => $state->id()
		);

    }

    public function getInventory($category_id) {

		# get selected category to load the listing
    	Doo::loadModel('Category');
    	$cat = new Category();
    	$cat->category_id = $category_id;
    	$cat = Doo::db()->getOne($cat);
    	$cat = empty($cat) ? 0 : $cat->id();
    	Doo::loadModel('PhysicalCopy');
    	$list = PhysicalCopy::getCategoryListingAsArray($cat);
    	$list = empty($list) ? array() : $list;
    	$current_category = $cat;

		# get the category listing for a select box
		$cat = new Category();
		$categories = array();
		foreach(Doo::db()->find($cat) as $cat) {
			array_push($categories, $cat->as_array());
		}
		usort($categories, function($a, $b) {
			return strnatcmp($a['fullName'], $b['fullName']);
		});


    	$books = array();

		foreach ($list as $copy) {

			$state_id = $copy->getCurrentState();
			if (! empty($state_id))
				$state_id = $state_id->state_id;
			else
				$state_id = 0;
			array_push($books, array(
				'id' => $copy->id(),
				'signature' => $copy->signature,
				'title' => $copy->Title->title,
				'author' => implode(', ', $copy->Title->getAuthorArray()),
				'state' => $state_id
			));
		}

		# get all states
		Doo::loadModel('PhysicalCopyState');
		$states = PhysicalCopyState::getStateArray();

    	return array(
   			'books'=>$books,
   			'categories'=>$categories,
   			'current_category' => $current_category,
   			'states' => $states
    	);

    }
}