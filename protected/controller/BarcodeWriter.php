<?php
class BarcodeWriter extends DooController{

	public function write_barcode() {

		# get id and make sure it's safe
		$id = $this->params['id'];
		if (! preg_match('/^[1-9][0-9]{0,4}$/', $id))
			return array('/error', 'internal');

		# hit the database
		Doo::loadModel('PhysicalCopy');
		$copy = new PhysicalCopy();
		$copy->id_physical_copy = $id;
		$copy = Doo::db()->getOne($copy);
		if (empty($copy))
			return array('/error', 'internal');

		# let's render it: make upper-case, replace invalid chars
		#$name = $copy->signature;
		#$name = preg_replace("/[^A-Z0-9 ]/", "-", $name);
		$name="LAV" . sprintf("%05d", $copy->id());
		$this->render_barcode($name);

	}

	public function render_barcode($data) {
		include(Doo::conf()->PROTECTED_FOLDER .'lib/barcode.php');
		$bc = new barcode(40);
		$bc->draw($data);
	}
}