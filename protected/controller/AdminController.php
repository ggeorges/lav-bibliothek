<?php
/**
 * MainController
 * Feel free to delete the methods and replace them with your own code.
 *
 * @author darkredz
 */

Doo::loadController('LAVcontroller');

class AdminController extends LAVcontroller{

    public function editStorageLocations() {

    	Doo::loadModel('StorageLocation');

    	# init
    	$location = Null;
    	$data = array();
    	$data['selected_id'] = 0;

    	if ($this->params['loc']=='new') {

    		if ($_REQUEST['typeCode']=='2') {

    			if (! isset($_REQUEST['n']))
    				throw Exception("n must be set");
    			$n = $_REQUEST['n'];
    			if (! isset($_REQUEST['m']))
    				throw Exception("m must be set");
    			$m = $_REQUEST['m'];


    			if (! is_numeric($n))
    				throw Exception("n must be numeric");
    			if (! is_numeric($m))
    				throw Exception("m must be numeric");

				$location = new StorageLocation();
				$location->id_storage_location = Doo::db()->insert($location);
				$location->turnIntoRack($n, $m);

    		}

    	} else if (preg_match('/^(?P<id>[1-9][0-9]*)(?:\.(html|json))?$/', $this->params['loc'], $matches)) {
	    	$location = new StorageLocation();
	    	$location->id_storage_location = (int) $matches['id'];
	        $location = Doo::db()->find($location);
	        if (empty($location))
	        	return array('/error', 'internal');
	        $location = $location[0];

	        $data['selected_id'] = $location->id();

	        // is this an update request?
	        if (isset($_POST['action'])) {
	        	switch ($_POST['action']) {
	        		case 'update':
						$location->display_name = $_POST['section_name'];
						if ($location->type_code==4) {
							$location->dimension_i = $_POST['section_width'];
							$location->dimension_j = 0;
						}
						Doo::db()->update($location);
						break;

	        		case 'create':
	        			if ($location->type_code == 3) {
	        				$section = new StorageLocation();

	        				// first, get maximum existing inner index
	        				$section->type_code = 4;
	        				$section->parent_location = $location->id();


	        				$section->display_name = $_POST['section_name'];
	        				$section->dimension_i = $_POST['section_width'];
	        				$section->dimension_j = 0;
	        				$section->inner_index = $location->getHighestChildInnerIndex()+1;
							Doo::db()->insert($section);
							$section->id_storage_location = Doo::db()->lastInsertId();
	        			}
	        			break;

	        		case 'delete':
	        			if ($location->type_code==4) {

	        				// goto parent
	        				$section = $location;
	        				while($location->type_code > 2) {
	        					$location = $location->getParent();
	        				}
	        				Doo::db()->delete($section);
	        			}
	        			break;

	        	}

	        }


	        if ($this->extension=='.html') {
	        	while($location->type_code > 2) {
		        	$location = $location->getParent();
	        	}
	        }

    	}

    	if ($location instanceof StorageLocation) {
    		$data['name'] = $location->display_name;
    		if ($this->extension=='.html')
    			$data['parents'] = $location->getParentsArray();
    		$data['type_code'] = $location->type_code;
    		switch ($location->type_code) {
    			case 2:
    				$data['rack'] = array('n' => $location->dimension_i, 'm' => $location->dimension_j);
    				$data['compartments'] = $location->getChildrenAsArray(True);
    				break;

  				case 3:
   					$data['inner_index'] = $location->inner_index;
   					$rack_width = $location->getParent()->dimension_j;
   					$data['position_j'] = (($location->inner_index-1) % $rack_width) + 1;
   					$data['position_i'] = ($location->inner_index - $data['position_j'])/$rack_width + 1;
   					break;

    			case 4:
    				$data['width'] = $location->dimension_i;
    				break;

    		}
    	}

    	header('HTTP/1.1 200 OK');
    	switch ($this->extension) {
    		case '.json':
    			$this->toJSON($data, true);
    			break;
    		case '.html':
    			$this->renderc('storage_location_editor', $data);
    			break;
    		default:
    			return array('/error', 'internal');
    			break;
    	}

    }
}